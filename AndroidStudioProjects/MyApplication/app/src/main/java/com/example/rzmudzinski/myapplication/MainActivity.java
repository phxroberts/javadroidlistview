package com.example.rzmudzinski.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView listView = (ListView)findViewById(R.id.MainListView);
        ListItemsAdapter listAdapter = new ListItemsAdapter(this,new String [] {
                "First","Second","Third","Fourth","Fifth","Sixth","Seventh","Eight","Ninth","Tenth"
        });
        listView.setAdapter(listAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

class ListItemsAdapter extends BaseAdapter implements View.OnClickListener{

    Context m_Context;
    String [] m_Data;
    public ListItemsAdapter(Context contextInstance, String [] data) {
        m_Context=contextInstance;
        m_Data=data;
    }
    @Override
    public int getCount() {
        return m_Data.length;
    }

    @Override
    public Object getItem(int i) {
        return m_Data[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = view;
        ListItemAdapterViewHolder viewHolder = view==null ? null : (ListItemAdapterViewHolder)view.getTag();
        if(v==null) {
            v = LayoutInflater.from(m_Context).inflate(R.layout.list_item,viewGroup,false);
            v.setTag(viewHolder=new ListItemAdapterViewHolder());
            viewHolder.Item=(TextView)v.findViewById(R.id.listItem);
            viewHolder.ItemReversed=(TextView)v.findViewById(R.id.listItemReversed);
            viewHolder.ClickMe=(Button)v.findViewById(R.id.clickme);
            viewHolder.ClickMe.setOnClickListener(this);
        }
        viewHolder.Item.setText(m_Data[i]);
        viewHolder.ItemReversed.setText(new StringBuffer(m_Data[i]).reverse().toString());
        viewHolder.ClickMe.setTag(viewHolder);
        return v;
    }
    public void onClick(View v) {
        ListItemAdapterViewHolder viewHolder = (ListItemAdapterViewHolder)v.getTag();
        Toast.makeText(m_Context,viewHolder.Item.getText(),Toast.LENGTH_SHORT).show();
    }
}

class ListItemAdapterViewHolder {
    public TextView Item;
    public TextView ItemReversed;
    public Button ClickMe;
}